const data = require("./1-arrays-jobs.cjs");

const result = data.reduce((acc, datas) => {
  if (datas.job.startsWith("Web Developer")) {
    acc.push(datas);
  }
  return acc;
}, []);

console.log(result);
