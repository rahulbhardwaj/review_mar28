const data = require("./1-arrays-jobs.cjs");

const result = data.reduce((acc, people) => {
  let salary = people.salary;
  salary = parseFloat(salary.slice(1, salary.length));
  const country = people.location;
  if (acc[country]) {
    const xcount = acc[country].countof;
    const xsalary = acc[country].avg_salary;
    acc[country].avg_salary = (xsalary + salary) / (xcount + 1);
    acc[country].countof = xcount + 1;
  } else {
    acc[country] = { avg_salary: salary, countof: 1 };
  }
  return acc;
}, {});

console.log(result);
