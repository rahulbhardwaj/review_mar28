const data = require("./1-arrays-jobs.cjs");

const result = data.reduce((acc, people) => {
  let salary = people.salary;
  salary = parseFloat(salary.slice(1, salary.length));
  const country = people.location;
  if (acc[country]) {
    acc[country] += salary;
  } else {
    acc[country] = salary;
  }
  return acc;
}, {});

console.log(result);
